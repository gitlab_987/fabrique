from django.contrib import admin
from django.urls import path, include
from rest_framework.schemas import get_schema_view
from django.views.generic import TemplateView


urlpatterns = [
    path(
        "swagger-ui/",
        TemplateView.as_view(
            template_name="/app/msglist/templates/swagger-ui.html",
            extra_context={"schema_url": "openapi-schema"},
        ),
        name="swagger-ui",
    ),
    path(
        "openapi",
        get_schema_view(
            title="Your Project", description="API for all things …", version="1.0.0"
        ),
        name="openapi-schema",
    ),
    # ...
    path("admin/", admin.site.urls),
    # path("api/", include("msglist.urls")),
    path("", include("msglist.urls")),
]
