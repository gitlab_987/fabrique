import requests
import logging


class Probe:
    """send requests to probe server"""

    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NzQ3MzExMTgsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IkRtaXRyeS5Qb2x5YWtvdiJ9.1cXXHZoHULcgbtwr6PhZZCGasYe6Ut26zEq9TOxvgpc"

    def __init__(self, id: int, phone: str, text: str) -> None:
        self.id = id
        self.phone = phone
        self.text = text

    def send(self) -> dict:

        """make request"""
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {self.token}",
        }
        payload = {"id": self.id, "phone": self.phone, "text": self.text}
        url = f"https://probe.fbrq.cloud/v1/send/{self.id}"

        try:
            req = requests.post(url, headers=headers, json=payload)
            req.raise_for_status()
        except requests.exceptions.HTTPError as err:
            logging.info(
                f"Probe Server returns {req.status_code} code for message id:{self.id}"
            )
            logging.error("Http Error:", err)
        except requests.exceptions.ConnectionError as err:
            logging.error("Error Connecting:", err)
        except requests.exceptions.Timeout as err:
            logging.error("Timeout Error:", err)
        except requests.exceptions.RequestException as err:
            logging.error("Something Else happens", err)
        else:
            logging.info(
                f"Probe Server returns {req.status_code} code for message id:{self.id}"
            )
            if req.status_code in [200, 201]:
                res = req.json()
                if res["message"] == "OK":
                    return True

        return False


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    p = Probe(2, "78888888888", "test text")
    res = p.send()
    print(res)
