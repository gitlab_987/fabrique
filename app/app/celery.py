import os
from celery import Celery
import random
from celery.utils.log import get_task_logger
from .probe import Probe


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")

app = Celery("app")
app.config_from_object("django.conf:settings", namespace="CELERY")

logger = get_task_logger(__name__)


@app.task(bind=True)
def send_messages(self):
    import datetime
    from django.db import connection
    from django.utils.timezone import get_current_timezone
    from msglist.models import Message

    sql = """
        SELECT c.id, s.id, s.text, phone, m.id, status FROM client c
        LEFT JOIN message_to_send s 
        ON c.tag_str=s.filter_tag_str AND c.op_code_str=filter_op_code_str
        LEFT JOIN message m ON m.client_id=c.id AND m.mailing_list_id=s.id
        WHERE s.id IS NOT NULL and 
		(m.id IS NULL OR m.status='FL')
		AND NOW() BETWEEN start AND final_time
        """
    name_map = ("cid", "sid", "text", "phone", "mid", "status")

    logger.info("starting message creating process")

    with connection.cursor() as cursor:
        cursor.execute(sql)
        while True:
            row = cursor.fetchone()
            if row:
                res = dict(zip(name_map, row))
                # послать сообщение
                # создать запись о сообщении
                now = datetime.datetime.now(tz=get_current_timezone())
                if res["mid"]:
                    msg = Message.objects.get(pk=res["mid"])
                else:
                    msg = Message(
                        created_at=now,
                        status="NW",
                        client_id=res["cid"],
                        mailing_list_id=res["sid"],
                    )
                    msg.save()

                logger.info(f"new message created id:{msg.id}")
                probe = Probe(msg.id, res["phone"], res["text"])
                res_probe = probe.send()
                if res_probe:
                    msg.status = "ST"
                    logger.info(f"message sent {msg.id}")
                else:
                    msg.status = "FL"
                    logger.info(f"message failed {msg.id}")
                msg.save()
            else:
                # nothing to send anymore
                break
    return 1


# start send_messages each 10 sec
@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(10.0, send_messages.s(), name="every 10")
    logger.info("setup periodic tasks")


app.autodiscover_tasks()
