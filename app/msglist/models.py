from statistics import mode
from django.db import models

from django.core.validators import MinValueValidator, MaxValueValidator
from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError

import pytz
from pytz.exceptions import UnknownTimeZoneError


# Create your models here.


def validate_time_zone(value):
    try:
        time_zone = pytz.timezone(value)
    except UnknownTimeZoneError:
        raise ValidationError("wrong format of time zone ex. Australia/Melbourne")

    return value


op_code_regex = RegexValidator(
    regex=r"^[0-9]{3,3}$",
    message="opcode value format: 000..999",
)


class MessageToSend(models.Model):
    start = models.DateTimeField()
    text = models.CharField(max_length=255)
    filter_tag_str = models.CharField(max_length=100, default="", blank=False)

    filter_op_code_str = models.CharField(
        max_length=3, validators=[op_code_regex], blank=False, default="123"
    )
    final_time = models.DateTimeField()

    class Meta:
        db_table = "message_to_send"


class Client(models.Model):
    phone_regex = RegexValidator(
        regex=r"^7[0-9]{10,10}$",
        message="Phone number must be entered in the format: '79999999999'. 11 digits allowed.",
    )

    phone = models.CharField(max_length=11, blank=False, validators=[phone_regex])

    tag_str = models.CharField(max_length=100, default="", blank=False)
    op_code_str = models.CharField(
        max_length=3, validators=[op_code_regex], blank=False, default="123"
    )

    time_zone = models.CharField(
        max_length=40, validators=[validate_time_zone], default="Australia/Melbourne"
    )

    def __str__(self):
        return self.phone

    class Meta:
        db_table = "client"


class Statuses(models.TextChoices):
    NEW = "NW", _("New")
    SENT = "ST", _("Sent")
    FAILED = "FL", _("Failed")
    CANCELED = "CL", _("Canceled")


class Message(models.Model):

    created_at = models.DateTimeField()
    status = models.CharField(
        max_length=2, choices=Statuses.choices, default=Statuses.NEW
    )

    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="clients")
    mailing_list = models.ForeignKey(
        MessageToSend, on_delete=models.CASCADE, related_name="messages"
    )

    class Meta:
        db_table = "message"
