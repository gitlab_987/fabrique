from django.apps import AppConfig


class MsglistConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'msglist'
