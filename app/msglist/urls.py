from django.urls import path, include
from rest_framework import routers
from rest_framework.schemas import get_schema_view


from . import api
from . import views


app_name = "msglist"

router = routers.DefaultRouter()
router.register(r"client", api.ClientViewSet)
router.register(r"messagetosend", api.MessageToSendViewSet)



urlpatterns = [
    path("", include(router.urls)),

    path("stats", api.StatsApiView.as_view()),
    path("stats/<int:id>", api.StatsApiView.as_view()),

    
]
