from . import models
from rest_framework import serializers


class MessageToSendSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MessageToSend
        fields = "__all__"


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Message
        fields = "__all__"




class StatSerializer(serializers.ModelSerializer):
    messages_total_count = serializers.SerializerMethodField()
    messages_new_count = serializers.SerializerMethodField()
    messages_sent_count = serializers.SerializerMethodField()
    messages_fail_count = serializers.SerializerMethodField()

    def get_messages_total_count(self, obj):
        return obj.messages.count()

    def get_messages_sent_count(self, obj):
        return obj.messages.filter(status="ST").count()

    def get_messages_new_count(self, obj):
        return obj.messages.filter(status="NW").count()

    def get_messages_fail_count(self, obj):
        return obj.messages.filter(status="FL").count()

    class Meta:
        model = models.MessageToSend
        fields = (
            "id",
            "start",
            "text",
            "final_time",
            "filter_tag_str",
            "filter_op_code_str",
            "messages_total_count",
            "messages_new_count",
            "messages_sent_count",
            "messages_fail_count",
        )
