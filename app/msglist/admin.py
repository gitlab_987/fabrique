from django.contrib import admin
from django.contrib.auth.models import User, Group
from .models import Message, Client, MessageToSend
# from .models import Tag, OpCode

# Register your models here.

# admin.site.register(Message)
# admin.site.register(Tag)
# admin.site.register(OpCode)
# admin.site.register(Client)
# admin.site.register(MessageToSend)

admin.site.unregister(User)
admin.site.unregister(Group)


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    # list_display = ("phone", "op_code", "op_code_str", "tag", "tag_str", "time_zone")
    list_display = ("phone", "op_code_str", "tag_str", "time_zone")


@admin.register(MessageToSend)
class MessageToSendAdmin(admin.ModelAdmin):
    list_display = (
        "text",
        "filter_op_code_str",
        "filter_tag_str",
        "start",
        "final_time",
    )

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ("id", "status", "client_id", "mailing_list_id")
