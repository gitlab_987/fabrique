from rest_framework.generics import ListAPIView
from rest_framework.viewsets import ModelViewSet
from . import serializers
from . import models


class ClientViewSet(ModelViewSet):
    serializer_class = serializers.ClientSerializer
    queryset = models.Client.objects.all()


class MessageToSendViewSet(ModelViewSet):
    serializer_class = serializers.MessageToSendSerializer
    queryset = models.MessageToSend.objects.all()


class StatsApiView(ListAPIView):
    serializer_class = serializers.StatSerializer

    def get_queryset(self):
        id = self.kwargs.get("id")
        if id is None:
            queryset = models.MessageToSend.objects.all()
        else:
            queryset = models.MessageToSend.objects.filter(id=id).all()
        return queryset
