# Сервис уведомлений
Тестовое задание для кандидатов-разработчиков python


### Api Documentatuon

http://127.0.0.1/swagger-ui/

http://127.0.0.1/openapi

 

### Getting started
just start 

    docker-compose up --build


### For debuging
http://127.0.0.1:5050 - pgAdmin

http://127.0.0.1:8888 - Flower

http://127.0.0.1/admin/ 

to enter admin panel need to create admin user

    docker exec -it <app_name>_app_1 python manage.py createsuperuser
