Django>=3.2
celery>5
redis==3.4.1
psycopg2>=2.7.5
djangorestframework
markdown

pyyaml # need for swagger
uritemplate # need for swagger
requests
